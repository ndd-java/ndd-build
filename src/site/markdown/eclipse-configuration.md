# Eclipse configuration

The Eclipse plugins and their configuration to get a compliant NDD workspace. Well, that's more of a receipe for myself... ;-)

The directory `<NDD_BUILD_DIR>` reffers to the `ndd-build` project directory.

In order for Eclipse to ignore some plugins, some profiles must be activated. The easiest way is to add the property `eclipse.running` to `eclipse.ini`:

```
-vmargs
-Declipse.running=true
```



## Plugins installation

Install from the marketplace the following plugins:

* Eclipse CS
* ECL Emma
* FindBugs
* JSON Tools
* TestNG
* YEdit

Install from their update site:

* Shell Ed [(update site)](http://sourceforge.net/projects/shelled/files/shelled/update/)

NB: Some dependencies are embedded inside the Eclipse TestNG JAR and may conflict with some project dependencies. The conflicting JAR are so far : `snakeyaml`. A workaround is to open the **project** properties then TestNG and check `Use project TestNG JAR`.



## Preferences

In `Window > Preferences`:

### General

* Appearance
    * `Theme = classic`
    * Editors
        * Text editors
            * `insert spaces for tab`
            * `print margin column` = `120`
            * `show line numbers`
            * Spelling
                * `Enable spell checking`
                * `User defined dictionary` = `<NDD_BUILD_DIR>/src/tools/eclipse/ndd-java.dic`
    * Keys
        * `next editor` = `CTRL + TAB`
        * `previous editor` = `CTRL + SHIFT + TAB`
        * `duplicate line` = `CTRL + SHIFT + D`
        * `quick fix` = `CTRL + &`
    * Network connections
        * `Proxies` = `manual (if needed)`
    * User Storage Service
        * `User name`
        * `Password`
    * Web browser
        * `use external web browser`
    * Workspace
        * `refresh using native hooks or polling`

### Checkstyle

* `Include rule names in violation messages`
* `Run checkstyle in background on full build`

Creates a _main_ configuration :

1. New
2. Type : `External configuration`
3. Name : `NDD Checks`
4. Location : `<NDD_BUILD_DIR>/src/tools/checkstyle/checkstyle-main.xml`
5. Additional properties : `checkstyle.dir` = `${basedir}/src/tools/checkstyle`
6. OK

Creates a _test_ configuration in `Preferences > Checkstyle` :

1. New
2. Type : `External configuration`
3. Name : `NDD Test Checks`
4. Location : `<NDD_BUILD_DIR>/src/tools/checkstyle/checkstyle-test.xml`
5. Additional properties : `checkstyle.dir` = `${basedir}/src/tools/checkstyle`
6. OK

### Install/Update

* Automatic updates
    * `Automatically find new updates`

### Java

* Appearance
    * Abbreviate package names
        * `name.didier.david={NDD}`
* Build path
    * `Source folder name` = `src/main/java`
    * `Output folder name` = `target/classes`
* Code style
    * Clean up : import `<NDD_BUILD_DIR>/src/tools/eclipse/java-code-cleaner.xml`
    * Formatter : import `<NDD_BUILD_DIR>/src/tools/eclipse/java-code-formatter.xml`
    * Organize imports : import `<NDD_BUILD_DIR>/src/tools/eclipse/java-code-imports.xml`
* Compiler
    * Compiler compliance level = `1.8`
    * Errors/Warning : all at `Warning` level except
        * `Unqualified access to instance field` = `Ignore`
        * `Non-externalized strings` = `Ignore`
        * `Method can be static` = `Ignore`
        * `Method can potentially be static` = `Ignore`
        * `Boxing and unboxing conversions` = `Ignore`
        * `Forbidden reference` = `Error`
        * `Null pointer access` = `Error`
        * `Potential null pointer access` = `Error`
        * `Missing @NonNullNyDefault annotation on package` = `Ignore`
    * Javadoc
        * `Malformed Javadoc comment` = Warning
            * `Only consider members as visible as` = `Public`
            * `Validate tag arguments`
            * `Missing tag description` = `Validate all standard tags`
        * `Missing Javadoc tags` = `Warning`
            * `Only consider members as visible as` = `Public`
            * `Ignore in overriding and implementing methods`
            * `Ignore method type parameters`
        * `Missing Javadoc comments` = `Ignore`
* Debug
    * Step filtering
        * Use step filters:
            * `java.lang.ClassLoader`
* Editor
    * Content assist
        * `Completion overwrites`
        * `Insert best guessed arguments`
    * Favorites
        * As a readable list:
            * `com.google.common.base.Objects.*`
            * `com.google.common.base.Throwables.*`
            * `com.google.common.collect.Lists.*`
            * `com.google.common.collect.Maps.*`
            * `com.google.common.collect.Sets.*`
            * `com.google.common.io.Files.*`
            * `com.google.common.io.IOUtils.*`
            * `java.lang.String.*`
            * `java.nio.charset.StandardCharsets.*`
            * `java.util.Arrays.*`
            * `java.util.concurrent.TimeUnit.*`
            * `name.didier.david.check4j.ConciseCheckers.*`
            * `name.didier.david.test4j.testng.TestNgDataProviders.*`
            * `org.apache.commons.lang3.StringUtils.*`
            * `org.assertj.core.api.Assertions.*`
            * `org.mockito.Matchers.*`
            * `org.mockito.Mockito.*`
        * Or in `.metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.jdt.ui.prefs` add to the `content_assist_favorite_static_members` key : `com.google.common.base.Objects.*;com.google.common.base.Throwables.*;com.google.common.collect.Lists.*;com.google.common.collect.Maps.*;com.google.common.collect.Sets.*;com.google.common.io.Files.*;com.google.common.io.IOUtils.*;java.lang.String.*;java.nio.charset.StandardCharsets.*;java.util.Arrays.*;java.util.concurrent.TimeUnit.*;name.didier.david.check4j.ConciseCheckers.*;name.didier.david.test4j.testng.TestNgDataProviders.*;org.apache.commons.lang3.StringUtils.*;org.assertj.core.api.Assertions.*;org.mockito.Matchers.*;org.mockito.Mockito.*`
    * Save actions
        * Perform selected actions on save
            * `Format source code (all lines)`
            * `Organize imports`
            * `Additional actions` : same as `Java > Code style > Clean up`
    * Templates : import `<NDD_BUILD_DIR>/src/tools/eclipse/java-editor-templates.xml`

### Java Persistence

TODO

### JSON

* JSON Files
    * Editor
        * Formatter
            * `Line width` = `120`
            * `Indent using spaces`
            * `Indentation size` = `2`

### Maven

* `Download Artifact Sources`
* `Download Artifact JavaDoc`
* `Download repository index updates on startup`
* `Update Maven projects on startup`
* Errors/Warnings
    * `groupID duplicate or parent groupId` = `Ignore`
    * `version duplicate or parent version` = `Ignore`
* Installations
    * Add external...
* User interface
    * `Open XML page in the POM editor by default`

### Run/Debug

* Console
    * `Limit console output` = unchecked
* Launching
    * `Launch operation` : `Always launch the previously launched application`

### Shell Script

* Editor
    * `Tab policy` = `Spaces only`

### TestNG

* `Output directory` = `target/test-classes`

### XML

* XML Files
    * Editor
        * `Line width` = `120`
        * `Indent using spaces`
        * `Indentation size` = `2`

### YEdit

* Formatter preferences
    * `Line width` = `120`





## Perspectives and Views

In `Window > Perspective > Customize perspective`:

* `Toolbar visibility`
    * `Window Working Set`
        * `Working Sets`

In each `View menu`:

* select the `Window Working Set`
